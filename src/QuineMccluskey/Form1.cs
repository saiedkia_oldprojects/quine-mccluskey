using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using QM;
using QuineMccluskey;

namespace WindowsApplication1
{
    // Saied Salarkia
    // Islamic Azad Univercity Shahre Rey Branch
    // may.2011 - Ordibehesht.1390
    public partial class frmMain : Form
    {
        qmAlg QMInstance;
        PictureBox[] pic;
        CheckedListBox[] lstTerms;
        Control frmRef;
        Color clrZero = Color.White;
        Color clrOne = Color.Silver;

        public frmMain()
        {
            InitializeComponent();
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            Init();
            HideList();
            rdTwo.Select();
            SortPic(2);
        }
        private void Init()
        {
            frmRef = this;

            

            Font fnt = new Font(FontFamily.GenericSansSerif, 12);
            lstTerms = new CheckedListBox[7];

            for (int i = 0; i < 7; i++)
            {
                lstTerms[i] = new CheckedListBox();

                lstTerms[i].Parent = frmRef;
                lstTerms[i].Visible = true;
                lstTerms[i].Enabled = false;
                lstTerms[i].BorderStyle = BorderStyle.FixedSingle;
                lstTerms[i].Width = 95;
                lstTerms[i].Height = frmRef.Height - lstTerms[i].Top - 50;
                lstTerms[i].Font = fnt;
                if (i > 0)
                    lstTerms[i].Left = lstTerms[i - 1].Left + lstTerms[i].Width + 2;
                else
                    lstTerms[i].Left = 600;
            }
        }

        #region PictureBox

        private void FunctionDesigner(int NrVar)
        {
            int NrCols = 0, NrLins = 0;

            if ((NrVar == 2) || (NrVar == 4)) { QMInstance = new qmAlg(NrVar, NrVar, NrVar); }
            if (NrVar == 3) { QMInstance = new qmAlg(NrVar, 2, 4); }
            if (NrVar == 5) { QMInstance = new qmAlg(NrVar, 8, 4); };
            if (NrVar == 6) { QMInstance = new qmAlg(NrVar, 8, 8); };

            NrCols = QMInstance.NrCols;
            NrLins = QMInstance.NrLins;

            if (pic != null)
                for (int i = 0; i < pic.Length; i++)
                {
                    if (pic[i] != null)
                    {
                        pic[i].Top = -500;
                        pic[i].Visible = false;
                    }
                }

            pic = new PictureBox[QMInstance.NrCols * QMInstance.NrLins];


            for (int j = 0; j < NrLins; j++)
            {
                for (int i = 0; i < NrCols; i++)
                {
                    casuta_noua(i, j);
                }
            }
            for (int j = 0; j < NrLins * NrCols; j++)
            {
                pic[j].Visible = true;
            }


        }
        private void HideList()
        {
            lstTerms[0].Visible = false;
            lstTerms[1].Visible = false;
            lstTerms[2].Visible = false;
            lstTerms[3].Visible = false;
            lstTerms[4].Visible = false;
            lstTerms[5].Visible = false;
            lstTerms[6].Visible = false;
            statusStrip1.Visible = false;
        }
        public void casuta_noua(int i, int j)
        {
            int NrCols = QMInstance.NrCols;
            int NrLins = QMInstance.NrLins;
            int height = 60;
            int width = 60;
            int top = 55;
            int left = 20;

            pic[i + j * NrCols] = new PictureBox();
            pic[i + j * NrCols].Parent = groupBox1;
            pic[i + j * NrCols].Visible = false;
            pic[i + j * NrCols].BorderStyle = (BorderStyle)1;

            pic[i + j * NrCols].BackColor = clrZero;
            pic[i + j * NrCols].Parent = frmRef;


            pic[i + j * NrCols].Width = width;
            pic[i + j * NrCols].Height = height;
            int extra = i / 4;
            extra = extra * 20;
            pic[i + j * NrCols].Left = 5 + left + i * (width + 5) + extra;

            extra = j / 4;
            extra = extra * 20;
            pic[i + j * NrCols].Top = 10 + top + (height + 5) * j + extra;


            pic[i + j * NrCols].Click += Pic_Clicked;
            pic[i + j * NrCols].MouseMove += Pic_MouseMove;

        }


        private void Pic_MouseMove(object sender, EventArgs e)
        {
            for (int i = 0; i < pic.Length; i++)
                if (pic[i].Equals(sender))
                {
                    statusStrip1.Items[0].Text = qmTools.binary(i, QMInstance.NrVar, QMInstance.NrCols, QMInstance.NrLins) + " " + qmTools.readableText(qmTools.binary(i, QMInstance.NrVar, QMInstance.NrCols, QMInstance.NrLins));
                    //////////
                    string[] todec = statusStrip1.Items[0].Text.Split(' ');
                    //int ConvtoDec = int.Parse(todec[0]);
                    //ConvtoDec = decimal.ToInt32(ConvtoDec);

                    val.Text = "Value: " + statusStrip1.Items[0].Text + "  ( " + ToDecimal(todec[0]) + " )";
                    toolTip.SetToolTip(this.pic[i], ToDecimal(todec[0]).ToString());
                    pic[i].BorderStyle = BorderStyle.None;
                }
                else
                {
                    pic[i].BorderStyle = BorderStyle.FixedSingle;
                }
        }
        public static int ToDecimal(string bin)
        {
            long l = Convert.ToInt64(bin, 2);
            int i = (int)l;
            return i;
        }

        private void Pic_Clicked(object sender, EventArgs e)
        {
            QMInstance.crtPoz = 0;
            for (int i = 0; i < 7; i++) lstTerms[i].Items.Clear();
            for (int i = 0; i < pic.Length; i++) if (pic[i].Equals(sender))
                {
                    QMInstance.term[i] = !QMInstance.term[i];
                    if (QMInstance.term[i]) pic[i].BackColor = clrOne;
                    else pic[i].BackColor = clrZero;
                    break;
                }

            ShowTerms();
        }
        private void ShowTerms()
        {
            lblTerms.Text = "Termeni: ";
            lstTerms[0].Items.Clear();
            for (int i = 0; i < pic.Length; i++)
                if (QMInstance.term[i])
                {
                    lblTerms.Text += " " + i.ToString();
                    lstTerms[0].Items.Add(qmTools.binary(i, QMInstance.NrVar, QMInstance.NrCols, QMInstance.NrLins));
                }
            lbt.Text = "";
            for (int i = 0; i <= lstTerms[0].Items.Count - 1; i++)
            {
                lbt.Text += lstTerms[0].Items[i].ToString() + System.Environment.NewLine;
            }
        }


        #endregion
        private void btbProcess_Click(object sender, EventArgs e)
        {
            QMInstance.ProcessInit();
            for (int crt = 0; crt <= QMInstance.crtPoz; crt++)
            {
                lstTerms[crt].Items.Clear();
                for (int j = 0; j < QMInstance.etape[crt].Length; j++)
                    if (QMInstance.etape[crt][j] != null)
                        if (qmTools.numaraCaractere(QMInstance.etape[crt][j], '-') == crt)
                        {
                            lstTerms[crt].Items.Add(QMInstance.etape[crt][j]);
                            lstTerms[crt].SetItemChecked(lstTerms[crt].Items.IndexOf(QMInstance.etape[crt][j]), true);
                        }
                if (crt > 0)
                    foreach (string ss in QMInstance.selected[crt - 1])
                    {
                        lstTerms[crt - 1].SetSelected(lstTerms[crt - 1].Items.IndexOf(ss), true);
                        lstTerms[crt - 1].SetItemChecked(lstTerms[crt - 1].Items.IndexOf(ss), false);
                    }
            }

            lblFunction.Text = "f = 0 + ";
            QMInstance.FindFunction(QMInstance.etape);
            lblFunction.Text = QMInstance.function;
            txtRes.Text = "";
            for (int i = 0; i <= lstTerms[0].Items.Count - 1; i++)
            {
                if (lstTerms[0].Items[i].ToString() != "")
                {
                    txtRes.Text += lstTerms[0].Items[i].ToString() + System.Environment.NewLine;
                }
            }
            txtRes.Text += "-----------" + System.Environment.NewLine;
            for (int i = 0; i <= lstTerms[1].Items.Count - 1; i++)
            {
                if (lstTerms[1].Items[i].ToString() != "")
                {
                    txtRes.Text += lstTerms[1].Items[i].ToString() + System.Environment.NewLine;
                }
            }
            txtRes.Text += "-----------" + System.Environment.NewLine;
            for (int i = 0; i <= lstTerms[2].Items.Count - 1; i++)
            {
                if (lstTerms[2].Items[i].ToString() != "")
                {
                    txtRes.Text += lstTerms[2].Items[i].ToString() + System.Environment.NewLine;
                }
            }
            txtRes.Text += "-----------" + System.Environment.NewLine;
            for (int i = 0; i <= lstTerms[3].Items.Count - 1; i++)
            {
                if (lstTerms[3].Items[i].ToString() != "")
                {
                    txtRes.Text += lstTerms[3].Items[i].ToString() + System.Environment.NewLine;
                }
            }
            txtRes.Text += "-----------" + System.Environment.NewLine;
            for (int i = 0; i <= lstTerms[4].Items.Count - 1; i++)
            {
                if (lstTerms[4].Items[i].ToString() != "")
                {
                    txtRes.Text += lstTerms[4].Items[i].ToString() + System.Environment.NewLine;
                }
            }
            txtRes.Text += "-----------" + System.Environment.NewLine;
            for (int i = 0; i <= lstTerms[5].Items.Count - 1; i++)
            {
                if (lstTerms[5].Items[i].ToString() != "")
                {
                    txtRes.Text += lstTerms[5].Items[i].ToString() + System.Environment.NewLine;
                }
            }

        }

        private void frmMain_MouseMove(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < pic.Length; i++) pic[i].BorderStyle = (BorderStyle)1;
        }
        #region RdButtons
        private void rdTwo_CheckedChanged(object sender, EventArgs e)
        {
            FunctionDesigner(2);
            SortPic(2);
        }

        private void rdThree_CheckedChanged(object sender, EventArgs e)
        {
            FunctionDesigner(3);
            SortPic(3);
        }

        private void rdFour_CheckedChanged(object sender, EventArgs e)
        {
            FunctionDesigner(4);
            SortPic(4);
        }
        #endregion
        private void SortPic(int x)
        {
            if (x == 4)
            {
                //1
                pic[0].Top = 65;//0
                pic[0].Left = 30;
                pic[4].Top = 65;//1
                pic[4].Left = 30 + 65;
                pic[8].Top = 65;//3
                pic[8].Left = 30 + 130;
                pic[12].Top = 65;//2
                pic[12].Left = 30 + 195;
                ////2
                pic[1].Top = 65 * 2;//4
                pic[1].Left = 30;
                pic[5].Top = 65 * 2;//5
                pic[5].Left = 30 + 65;
                pic[9].Top = 65 * 2;//7
                pic[9].Left = 30 + 130;
                pic[13].Top = 65 * 2;//6
                pic[13].Left = 30 + 195;
                ////3
                pic[2].Top = 65 * 3;//12
                pic[2].Left = 30;
                pic[6].Top = 65 * 3;//13
                pic[6].Left = 30 + 65;
                pic[10].Top = 65 * 3;//14
                pic[10].Left = 30 + 130;
                pic[14].Top = 65 * 3;//15
                pic[14].Left = 30 + 195;
                ////4
                pic[3].Top = 65 * 4;//8
                pic[3].Left = 30;
                pic[7].Top = 65 * 4;//9
                pic[7].Left = 30 + 65;
                pic[11].Top = 65 * 4;//11
                pic[11].Left = 30 + 130;
                pic[15].Top = 65 * 4;//10
                pic[15].Left = 30 + 195;
            }
            if (x == 3)
            {
                //1
                pic[0].Top = 65;//0
                pic[0].Left = 30;
                pic[1].Top = 65;//1
                pic[1].Left = 30 + 65;
                pic[3].Top = 65;//3
                pic[3].Left = 30 + 130;
                pic[2].Top = 65;//2
                pic[2].Left = 30 + 195;
                ////2
                pic[6].Top = 65 * 2;//4
                pic[6].Left = 30;
                pic[7].Top = 65 * 2;//5
                pic[7].Left = 30 + 65;
                pic[5].Top = 65 * 2;//7
                pic[5].Left = 30 + 130;
                pic[4].Top = 65 * 2;//6
                pic[4].Left = 30 + 195;
            }
            if (x == 2)
            {
                //1
                pic[0].Top = 65;//0
                pic[0].Left = 30;
                pic[2].Top = 65;//1
                pic[2].Left = 30 + 65;
                ////2
                pic[1].Top = 65 * 2;//2
                pic[1].Left = 30;
                pic[3].Top = 65 * 2;//3
                pic[3].Left = 30 + 65;
            }
        }
    }
}
