namespace WindowsApplication1
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btbProcess = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblTerms = new System.Windows.Forms.ToolStripStatusLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdThree = new System.Windows.Forms.RadioButton();
            this.rdFour = new System.Windows.Forms.RadioButton();
            this.rdTwo = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblFunction = new System.Windows.Forms.TextBox();
            this.val = new System.Windows.Forms.Label();
            this.lbt = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtRes = new System.Windows.Forms.TextBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btbProcess
            // 
            this.btbProcess.Location = new System.Drawing.Point(242, 12);
            this.btbProcess.Name = "btbProcess";
            this.btbProcess.Size = new System.Drawing.Size(96, 23);
            this.btbProcess.TabIndex = 3;
            this.btbProcess.Text = "Calculate";
            this.btbProcess.UseVisualStyleBackColor = true;
            this.btbProcess.Click += new System.EventHandler(this.btbProcess_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.lblTerms});
            this.statusStrip1.Location = new System.Drawing.Point(0, 480);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(690, 22);
            this.statusStrip1.TabIndex = 8;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // lblTerms
            // 
            this.lblTerms.Name = "lblTerms";
            this.lblTerms.Size = new System.Drawing.Size(0, 17);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Numer of variables:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdThree);
            this.groupBox1.Controls.Add(this.rdFour);
            this.groupBox1.Controls.Add(this.rdTwo);
            this.groupBox1.Controls.Add(this.btbProcess);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(8, 1);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 44);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Variables";
            // 
            // rdThree
            // 
            this.rdThree.AutoSize = true;
            this.rdThree.Location = new System.Drawing.Point(155, 18);
            this.rdThree.Name = "rdThree";
            this.rdThree.Size = new System.Drawing.Size(31, 17);
            this.rdThree.TabIndex = 18;
            this.rdThree.TabStop = true;
            this.rdThree.Text = "3";
            this.rdThree.UseVisualStyleBackColor = true;
            this.rdThree.CheckedChanged += new System.EventHandler(this.rdThree_CheckedChanged);
            // 
            // rdFour
            // 
            this.rdFour.AutoSize = true;
            this.rdFour.Location = new System.Drawing.Point(198, 18);
            this.rdFour.Name = "rdFour";
            this.rdFour.Size = new System.Drawing.Size(31, 17);
            this.rdFour.TabIndex = 19;
            this.rdFour.TabStop = true;
            this.rdFour.Text = "4";
            this.rdFour.UseVisualStyleBackColor = true;
            this.rdFour.CheckedChanged += new System.EventHandler(this.rdFour_CheckedChanged);
            // 
            // rdTwo
            // 
            this.rdTwo.AutoSize = true;
            this.rdTwo.Location = new System.Drawing.Point(117, 18);
            this.rdTwo.Name = "rdTwo";
            this.rdTwo.Size = new System.Drawing.Size(31, 17);
            this.rdTwo.TabIndex = 17;
            this.rdTwo.TabStop = true;
            this.rdTwo.Text = "2";
            this.rdTwo.UseVisualStyleBackColor = true;
            this.rdTwo.CheckedChanged += new System.EventHandler(this.rdTwo_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.lblFunction);
            this.groupBox2.Location = new System.Drawing.Point(382, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(276, 44);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Result:";
            // 
            // lblFunction
            // 
            this.lblFunction.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblFunction.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFunction.Location = new System.Drawing.Point(6, 16);
            this.lblFunction.Name = "lblFunction";
            this.lblFunction.ReadOnly = true;
            this.lblFunction.Size = new System.Drawing.Size(264, 19);
            this.lblFunction.TabIndex = 0;
            // 
            // val
            // 
            this.val.AutoSize = true;
            this.val.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.val.Location = new System.Drawing.Point(22, 48);
            this.val.Name = "val";
            this.val.Size = new System.Drawing.Size(47, 13);
            this.val.TabIndex = 13;
            this.val.Text = "Value :";
            // 
            // lbt
            // 
            this.lbt.AutoSize = true;
            this.lbt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.lbt.Location = new System.Drawing.Point(313, 61);
            this.lbt.Name = "lbt";
            this.lbt.Size = new System.Drawing.Size(39, 15);
            this.lbt.TabIndex = 14;
            this.lbt.Text = "0000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(313, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 15;
            this.label2.Text = "Selected :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(385, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(31, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Log :";
            // 
            // txtRes
            // 
            this.txtRes.Location = new System.Drawing.Point(388, 64);
            this.txtRes.Multiline = true;
            this.txtRes.Name = "txtRes";
            this.txtRes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRes.Size = new System.Drawing.Size(270, 369);
            this.txtRes.TabIndex = 17;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 502);
            this.Controls.Add(this.txtRes);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbt);
            this.Controls.Add(this.val);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Quine McCluskey ( Salarkia )";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseMove);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btbProcess;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel lblTerms;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox lblFunction;
        private System.Windows.Forms.Label val;
        private System.Windows.Forms.Label lbt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtRes;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.RadioButton rdThree;
        private System.Windows.Forms.RadioButton rdFour;
        private System.Windows.Forms.RadioButton rdTwo;
    }
}

