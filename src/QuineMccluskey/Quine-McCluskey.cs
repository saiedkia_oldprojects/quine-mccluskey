using System;
using System.Collections.Generic;
namespace QM
{

    // copy right: this class coped many years ago from codeproject,i dont have any reference to that article/project now
  
    public class qmTools
    {
        
        public static string binaryToString(String term)
        {
            string result = "";
            for (int i = 0; i < term.Length; i++)
                result += litera(term[i], i);
            if (result == "") result = "1";
            return result;
        }
        #region Quine McCluskey Algorithm

        public static bool eInAcoperire(string elemants, string[] colectie)
        {
            foreach (string e in colectie) if (e != null) if (e != "") if (eInTermen(elemants, e)) return true;
            return false;
        }

        public static bool eInTermen(string Mic, string alaMare)
        {
            for (int i = 0; i < Mic.Length; i++)
                if ((Mic[i] != alaMare[i]) && (alaMare[i] != '-')) return false;
            return true;
        }
        
        public static string litera(char c, int poz)
        {
            string[] variabila = { "x", "y", "z", "e", "d", "v" }; ;
            if (c == '-')
                return "";
            else
            {
                if (c == '1') return variabila[poz];
                else return variabila[poz] + "'";
            }
        }

        
        public static int numaraCaractere(string s, char c)
        {
            int cntr = 0;
            for (int i = 0; i < s.Length; i++) if (s[i] == c) cntr++;

            return cntr;
        }

        
        public static string readableText(string binar)
        {
            string[] v = { "x=", "y=", "z=", "e=", "d=", "v=" };
            string result = new string("  ".ToCharArray());
            for (int i = 0; i < binar.Length; i++)
            {
                result += v[i] + binar[i] + "; ";
            }

            return result;
        }

        
        public static String binary(int i, int NrVar, int NrCols, int NrLins)
        {
            string[] col_val_long = { "000", "010", "110", "100", "001", "011", "111", "101" };
            string[] col_val = { "00", "01", "11", "10" };
            string[] col_val_short = { "0", "1" };
            int col = i % NrCols;
            int lin = i / NrCols;

            if (NrVar == 2) return col_val_short[col] + col_val_short[lin];
            if (NrVar == 3) return col_val[lin] + col_val_short[col];
            if (NrVar == 4) return col_val[col] + col_val[lin];
            if (NrVar == 5) return col_val_long[col] + col_val[lin];
            return col_val_long[col] + col_val_long[lin];
        }

        
        public static void scoateTermen(string ss, string[] eta)
        {

            int i = 0;
            while ((eta[i] != null) && (eta[i] != ss)) i++;
            eta[i] = null;
        }

        public static void adaugaTermen(string ss, string[] eta)
        {
            int i = 0;
            while (eta[i] != null) i++;
            eta[i] = new string(ss.ToCharArray());
        }
    }



    public class qmAlg
    {
        public int NrVar;
        public int crtPoz;
        public int NrCols = 0, NrLins = 0;
        public bool[] term;
        //string[] minterms;
        int qr;
        public string[][] etape = new string[8][];
        public LinkedList<string> finale;
        public LinkedList<string>[] selected = new LinkedList<string>[8];
        public string function;

        public qmAlg(int v, int c, int l)
        {
            NrCols = c;
            NrVar = v;
            NrLins = l;
            term = new bool[NrCols * NrLins];
            qr = 0;
            etape[1] = new string[NrCols * NrLins];


        }

      
        public void ProcessInit()
        {
            string[] minterms = new string[NrCols * NrLins];
            qr = 0;
            for (int i = 0; i < NrLins * NrCols; i++)
            {
                if (term[i] == true) minterms[qr++] = qmTools.binary(i, NrVar, NrCols, NrLins);
            }

            int ii = 0;
            if (crtPoz == 0)
            {
                etape[0] = new string[qr];
                foreach (string s in minterms)
                {
                    if (s != null) etape[0][ii++] = s;
                }
                Array.Sort(etape[0], new comp());
            }
            selected[crtPoz] = new LinkedList<string>();

            while (reshape() == 1) ;
            FindFunction(etape);
        }
       
        private int reshape()
        {
            crtPoz++;
          
            int i = 0;
            comp cmp = new comp();
            int inde = 0;
            bool gasit = false;
            etape[crtPoz] = new string[NrCols * NrLins * NrCols];
            for (int j = 0; j < etape[crtPoz - 1].Length; j++)
                if (etape[crtPoz - 1][j] != null)
                    for (i = j + 1; i < etape[crtPoz - 1].Length; i++)
                    {
                        if (etape[crtPoz - 1][i] != null)
                        {
                            int k = cmp.Difference(etape[crtPoz - 1][i], etape[crtPoz - 1][j]);
                            if (k == 1)
                            {
                                gasit = true;
                                for (int h = 0; h < etape[crtPoz - 1][i].Length; h++)
                                    if (etape[crtPoz - 1][i][h] != etape[crtPoz - 1][j][h])
                                    {
                                        etape[crtPoz][inde] = etape[crtPoz - 1][i];
                                        etape[crtPoz][inde] = etape[crtPoz][inde].Remove(h, 1);
                                        etape[crtPoz][inde] = etape[crtPoz][inde].Insert(h, "-");
                                        inde++;
                                    }
                            }
                        }
                    }

            if (gasit)
            {
                selected[crtPoz] = new LinkedList<string>();
                foreach (String e in etape[crtPoz - 1])
                {
                    if ((e != null) && (!qmTools.eInAcoperire(e, etape[crtPoz])))
                    {
                        if (qmTools.numaraCaractere(e, '-') == crtPoz - 1) selected[crtPoz - 1].AddLast(e);
                        etape[crtPoz][inde++] = e;
                    }
                }
                eliminaDuplicati(etape[crtPoz]);

                return 1;
            }
            else
            {
                crtPoz--;

                return 0;

            }

        }

      
        public void eliminaDuplicati(string[] array)
        {
            for (int j = 0; j < etape[crtPoz].Length; j++)
                for (int i = j + 1; i < etape[crtPoz].Length; i++)

                    while ((etape[crtPoz][i] != null) && (etape[crtPoz][j] != null) && (etape[crtPoz][i] != "") && (etape[crtPoz][i].CompareTo(array[j]) == 0))
                    {
                        for (int ii = j; ii < array.Length - 1; ii++) array[ii] = array[ii + 1];
                    }
        }
        
        public void FindFunction(string[][] etap)
        {
            string[][] et = new string[etap.Length][];
            Array.Copy(etap, et, etap.Length);
            for (int i = 0; i < etap.Length; i++)
                if (etap[i] != null)
                {
                    et[i] = new string[etap[i].Length];
                    Array.Copy(etap[i], et[i], etap[i].Length);
                }
            int[] gasit = new int[qr];
            string[] poz = new string[qr];
            int[] cnt = new int[et[crtPoz].Length];
            string[] minterms = new string[NrCols * NrLins];
            qr = 0;
            for (int i = 0; i < NrLins * NrCols; i++)
            {
                if (term[i] == true) minterms[qr++] = qmTools.binary(i, NrVar, NrCols, NrLins);
            }
            for (int jk = 0; jk < qr; jk++)
            {
                string s = minterms[jk];
                gasit[jk] = 1;
                gasit[jk] = 0;
                for (int i = 0; i < et[crtPoz].Length; i++)
                {
                    if ((et[crtPoz][i] != null) && (s != null) && (qmTools.eInTermen(s, et[crtPoz][i])))
                    {
                        if (gasit[jk] == 0) poz[jk] = et[crtPoz][i];
                        gasit[jk]++;
                    }
                }
            }
            finale = new LinkedList<string>();
            for (int jk = 0; jk < qr; jk++)
            {
                if ((gasit[jk] == 1) && (minterms[jk] != null))
                {
                    for (int jj = 0; jj < qr; jj++)
                        if ((poz[jk] != null) && (minterms[jj] != null) && (qmTools.eInTermen(minterms[jj], poz[jk])))
                        {
                            minterms[jj] = null;
                        }
                    finale.AddLast(poz[jk]);
                    qmTools.scoateTermen(poz[jk], et[crtPoz]);

                }
            }
            int pozMax = 0;
            while (pozMax > -1)
            {
                for (int i = 0; i < et[crtPoz].Length; i++)
                {
                    cnt[i] = 0;
                    for (int jj = 0; jj < qr; jj++)
                        if ((et[crtPoz][i] != null) && (minterms[jj] != null) && (qmTools.eInTermen(minterms[jj], et[crtPoz][i])))
                            cnt[i]++;
                }
                int max = 0;
                pozMax = -1;
                for (int i = 0; i < et[crtPoz].Length; i++)
                {
                    if (cnt[i] > max)
                    {
                        max = cnt[i];
                        pozMax = i;
                    }
                }

                if (pozMax > -1)
                {
                    finale.AddLast(et[crtPoz][pozMax]);
                    for (int jj = 0; jj < qr; jj++)
                        if ((minterms[jj] != null) && (qmTools.eInTermen(minterms[jj], et[crtPoz][pozMax])))
                        {
                            minterms[jj] = null;
                        }
                    et[crtPoz][pozMax] = null;
                }
            }
            if (finale.Count > 0)
            {
                function = "";
                foreach (string ss in finale) function += qmTools.binaryToString(ss) + "+";
                function = function.Remove(function.Length - 1, 1);

            }
            else function = "0";

        }

        public class comp : IComparer<string>
        {
            public int Compare(string a, string b)
            {
                int a_counter = 0;
                int b_counter = 0;
                if ((a == null) && (b == null)) return 0;
                if (a == null) return 1;
                if (b == null) return -1;
                for (int i = 0; i < a.Length; i++) if (a[i] == '1') a_counter++;
                for (int j = 0; j < b.Length; j++) if (b[j] == '1') b_counter++;

                return a_counter - b_counter;
            }

            public int Difference(string a, string b)
            {
                int cnter = 0;
                for (int i = 0; i < a.Length; i++) if (a[i] != b[i]) cnter++;

                return cnter;
            }

        }


    }
        #endregion
}